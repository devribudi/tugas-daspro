-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 29, 2021 at 01:48 PM
-- Server version: 8.0.26
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mahasiswa`
--

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `No` int NOT NULL,
  `Nama` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Agama` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Kota_lahir` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Tgl_lhr` date DEFAULT NULL,
  `No_hp` varchar(13) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Alamat` text COLLATE utf8_unicode_ci,
  `Kategori` text COLLATE utf8_unicode_ci,
  `Foto` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`No`, `Nama`, `Agama`, `Kota_lahir`, `Tgl_lhr`, `No_hp`, `Alamat`, `Kategori`, `Foto`) VALUES
(1, 'Devri budi', 'Katholik', 'Bantul', '1992-02-17', '087845666600', 'Jogonalan Lor RT 01 Dusun VII Tirtonirmolo Kasihan Bantul', 'Mahasiswa', ''),
(2, 'kristina widi', 'Katholik', 'Bantul', '1992-12-29', '1234567891678', 'tegal krapyak no 1', 'mahasiswa', ''),
(3, 'Serapia widi', 'katholik', 'yogyakarta', '1992-10-30', '087812345678', 'Tegal krapyak', 'mahasiswa', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`No`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buku`
--
ALTER TABLE `buku`
  MODIFY `No` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
